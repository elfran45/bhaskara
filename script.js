
const coefA = document.querySelector('[data-coefA]');
const coefB = document.querySelector('[data-coefB]');
const coefC = document.querySelector('[data-coefC]');
const positiveDisplay = document.querySelector('[data-positive-root-display]');
const negativeDisplay = document.querySelector('[data-negative-root-display]');
const submitButton = document.querySelector('[data-submit-button]');

const bhaskara = {
    A: coefA,
    B: coefB,
    C: coefC,

    calculateVertex() {
        return (-(this.B.value) / (2 * this.A.value));
    },

    calculateRoot() {
        return (Math.pow((this.B.value * this.B.value) - (4 * this.A.value * this.C.value),
            1 / 2) / (2 * this.A.value));
    },

    calculatePositiveRoot() {
        return this.calculateVertex() + this.calculateRoot();
    },

    calculateNegativeRoot() {
        return this.calculateVertex() - this.calculateRoot();
    },

    updateDisplay() {
        positiveDisplay.textContent = `First Root: ${this.calculatePositiveRoot()}`;
        negativeDisplay.textContent = `Second Root: ${this.calculateNegativeRoot()}`;
    },

};

submitButton.addEventListener("click", () => {
    bhaskara.updateDisplay();
})

